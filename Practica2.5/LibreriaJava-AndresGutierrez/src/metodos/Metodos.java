package metodos;

public class Metodos {
	
	public static String invertirCadena(String string) {
		String cadenaInversa = "";
		for(int i = 0; i < string.length(); i++){		
			cadenaInversa = string.charAt(i) + cadenaInversa;
		}
		return cadenaInversa;
	}
	
	public static String palabraMasLarga(String cadena) {
		String palabraEncontrada;
		int inicioPalabra = 0;
		String palabraMayor = "";
		cadena = cadena + " ";
			
		for(int i = 0; i < cadena.length(); i++){
			if(cadena.charAt(i) == ' '){
				palabraEncontrada = cadena.substring(inicioPalabra, i+1);
				if(palabraMayor.length() < palabraEncontrada.length()){
					palabraMayor = palabraEncontrada;
				}
				inicioPalabra = i+1;
			}
		}
		return palabraMayor;
	}
	
	public static void abecedario(){
		for(int i = 'A'; i <= 'Z'; i++ ){
			for(int j = 'A'; j<= 'Z' ; j++){
				for(int k = 'A'; k <= 'Z'; k++){
					System.out.print((char)i);
					System.out.print((char)j);
					System.out.println((char)k);
				}
			}
		}	
	}
	
	public static void dibujarCuadrado(int lado) {
		for(int i = 0; i < lado; i++){
			for(int j = 0; j < lado; j++){
				System.out.print("X ");
			}
			System.out.println();
		}
	}
	
	public static void dibujarRectangulo(int alto, int ancho) {
		for(int i = 0; i < alto; i++){
			for(int j = 0; j < ancho; j++){	
				System.out.print("X ");
			}
			System.out.println();
		}
	}

}
