package enteros;

public class Enteros {
	
	public static void esPrimo(int numero){
		int cantidadDivisores = 0;
		for (int i = 1; i <= numero; i++) {
			if(numero % i == 0){
				cantidadDivisores++;
			}
		}
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
	}
	
	public static int aleatorio(int a){
		int numero = (int) Math.round(Math.random() * a + 1);
		return numero;
	}
	
	public static int potencia(int base, int exponente){
		int resultado = 1;
		
		for(int i = 0; i < exponente; i++){
			resultado = resultado * base;
		}
		return resultado;
	}
	
	public static int redondear(int a){
		if(a - (int)a >= 0.5){
			return (int)a + 1;
		}
		return (int)a;
	}
	
	public static int sumar(int a, int b){
		int c = a + b;
		return c;
	}
	
}
