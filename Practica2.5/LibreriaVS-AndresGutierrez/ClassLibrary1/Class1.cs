﻿using System;
using System.IO;

namespace ClassLibrary1
{
    public class Class1
    {
        public static void esPrimo(int numero)
        {
            int cantidadDivisores = 0;
            for (int i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    cantidadDivisores++;
                }
            }
            if (cantidadDivisores > 2)
            {
                System.Console.WriteLine("No lo es");
            }
            else
            {
                System.Console.WriteLine("Si lo es");
            }
        }

        public static int potencia(int a, int exponente)
        {
            int resultado = 1;

            for (int i = 0; i < exponente; i++)
            {
                resultado = resultado * a;
            }
            return resultado;
        }

        public static int redondear(int a)
        {
            if (a - (int)a >= 0.5)
            {
                return (int)a + 1;
            }
            return (int)a;
        }

        public static int sumar(int a, int b)
        {
            int c = a + b;
            return c;
        }

        public static int restar(int a, int b)
        {
            int c = a - b;
            return c;
        }
    }

    public class Class2
    {
        public static String invertirCadena(String cadena)
        {
            String cadenaInversa = "";
            for (int i = 0; i < cadena.Length; i++)
            {
                cadenaInversa = cadena[i] + cadenaInversa;
            }
            return cadenaInversa;
        }

        public static String palabraMasLarga(String cadena)
        {
            String palabraEncontrada;
            int inicioPalabra = 0;
            String palabraMayor = "";
            cadena = cadena + " ";

            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] == ' ')
                {
                    palabraEncontrada = cadena.Substring(inicioPalabra, i + 1);
                    if (palabraMayor.Length < palabraEncontrada.Length)
                    {
                        palabraMayor = palabraEncontrada;
                    }
                    inicioPalabra = i + 1;
                }
            }
            return palabraMayor;
        }

        public static void abecedario()
        {
            for (int i = 'A'; i <= 'Z'; i++)
            {
                for (int j = 'A'; j <= 'Z'; j++)
                {
                    for (int k = 'A'; k <= 'Z'; k++)
                    {
                        System.Console.WriteLine((char)i);
                        System.Console.WriteLine((char)j);
                        System.Console.WriteLine((char)k);
                    }
                }
            }
        }

        public static void dibujarCuadrado(int lado)
        {
            for (int i = 0; i < lado; i++)
            {
                for (int j = 0; j < lado; j++)
                {
                    System.Console.Write("X ");
                }
                System.Console.WriteLine();
            }
        }

        public static void dibujarRectangulo(int alto, int ancho)
        {
            for (int i = 0; i < alto; i++)
            {
                for (int j = 0; j < ancho; j++)
                {
                    System.Console.Write("X ");
                }
                System.Console.WriteLine();
            }
        }

    }
}
