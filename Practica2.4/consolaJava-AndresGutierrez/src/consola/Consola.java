package consola;

import java.util.Scanner;

class Consola {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		System.out.println("*********************");
		System.out.println("Opcion 1: Sumar.");
		System.out.println("Opcion 2: Restar.");
		System.out.println("Opcion 3: Multiplicar.");
		System.out.println("Opcion 4: Dividir.");
		System.out.println("*********************");
		System.out.println("Elige una opcion: ");
		int opcionElegida = lector.nextInt();
		System.out.println("Introduce un numero: ");
		int a = lector.nextInt();
		System.out.println("Introduce otro numero: ");
		int b = lector.nextInt();
		
		switch (opcionElegida) {
		case 1:
			System.out.println("El resultado es: " + sumar(a,b));
			break;
		case 2:
			System.out.println("El resultado es: " + restar(a,b));
			break;
		case 3:
			System.out.println("El resultado es: " + multiplicar(a,b));
			break;
		case 4:
			System.out.println("El resultado es: " + dividir(a,b));
			break;
		default:
			System.out.println("Opcion no valida.");
		}
		lector.close();
	}

	private static int sumar(int a, int b) {
		int c = a + b;
		return c;
	}
	
	private static int restar(int a, int b) {
		int c = a - b;
		return c;
	}
	
	private static int multiplicar(int a, int b) {
		int c = a * b;
		return c;
	}
	
	private static int dividir(int a, int b) {
		int c = a / b;
		return c;
	}
	
	

}
