﻿using System;    //espacios de nombres que estoy usando
using System.IO;
namespace Aplicacion.parte1 //espacio de nombres de 'miClase'
{
    public class miClase
    {
        public static void Main(String[] args)
        {
            System.Console.WriteLine("*********************");
            System.Console.WriteLine("Opcion 1: Sumar.");
            System.Console.WriteLine("Opcion 2: Restar.");
            System.Console.WriteLine("Opcion 3: Multiplicar.");
            System.Console.WriteLine("Opcion 4: Dividir.");
            System.Console.WriteLine("*********************");
            System.Console.WriteLine("Elige una opcion: ");
            int opcionElegida = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce un numero: ");
            int a = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce otro numero: ");
            int b = int.Parse(System.Console.ReadLine());

            switch (opcionElegida)
            {
                case 1:
                    System.Console.WriteLine("El resultado es: " + sumar(a, b));
                    break;
                case 2:
                    System.Console.WriteLine("El resultado es: " + restar(a, b));
                    break;
                case 3:
                    System.Console.WriteLine("El resultado es: " + multiplicar(a, b));
                    break;
                case 4:
                    System.Console.WriteLine("El resultado es: " + dividir(a, b));
                    break;
            }
            System.Console.WriteLine("Pulsa una tecla para salir");
            Console.ReadKey();
        }

        private static int sumar(int a, int b)
        {
            int c = a + b;
            return c;
        }

        private static int restar(int a, int b)
        {
            int c = a - b;
            return c;
        }

        private static int multiplicar(int a, int b)
        {
            int c = a * b;
            return c;
        }

        private static int dividir(int a, int b)
        {
            int c = a / b;
            return c;
        }
    }
}