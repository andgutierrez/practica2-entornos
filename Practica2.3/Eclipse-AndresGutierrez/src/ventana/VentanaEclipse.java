package ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JToolBar;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class VentanaEclipse extends JFrame {
	
	/**
	 * @author Andres Gutierrez
	 * @since 27/01/2018
	 */

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaEclipse.class.getResource("/images/save.png")));
		setTitle("MyAccounting");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 634, 445);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNuevoArchivo = new JMenuItem("Nuevo archivo");
		mnNewMenu.add(mntmNuevoArchivo);
		
		JMenuItem mntmAbrirArchivo = new JMenuItem("Abrir archivo");
		mnNewMenu.add(mntmAbrirArchivo);
		
		JMenuItem mntmGuardarArchivo = new JMenuItem("Guardar archivo");
		mnNewMenu.add(mntmGuardarArchivo);
		
		JMenuItem mntmNuevaVentana = new JMenuItem("Nueva ventana");
		mnNewMenu.add(mntmNuevaVentana);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnNewMenu.add(mntmSalir);
		
		JMenu mnNewMenu_1 = new JMenu("Editar");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmTet = new JMenuItem("Retroceder");
		mnNewMenu_1.add(mntmTet);
		
		JMenuItem mntmDeshacerRetroceso = new JMenuItem("Deshacer retroceso");
		mnNewMenu_1.add(mntmDeshacerRetroceso);
		
		JMenu mnEdicion = new JMenu("Edicion");
		mnNewMenu_1.add(mnEdicion);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnEdicion.add(mntmCortar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEdicion.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEdicion.add(mntmPegar);
		
		JMenuItem mntmPegadoEspecial = new JMenuItem("Pegado especial");
		mnEdicion.add(mntmPegadoEspecial);
		
		JMenu mnSeleccion = new JMenu("Seleccion");
		menuBar.add(mnSeleccion);
		
		JMenu mnLenguaje = new JMenu("Lenguaje");
		mnSeleccion.add(mnLenguaje);
		
		JMenuItem mntmEspaol = new JMenuItem("Espa\u00F1ol");
		mnLenguaje.add(mntmEspaol);
		
		JMenuItem mntmIngles = new JMenuItem("Ingles");
		mnLenguaje.add(mntmIngles);
		
		JMenuItem mntmFrances = new JMenuItem("Frances");
		mnLenguaje.add(mntmFrances);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Seleccionar todo");
		mnSeleccion.add(mntmNewMenuItem);
		
		JMenu mnOpciones = new JMenu("Opciones");
		menuBar.add(mnOpciones);
		
		JMenuItem mntmVolumen = new JMenuItem("Volumen");
		mnOpciones.add(mntmVolumen);
		
		JMenuItem mntmLetra = new JMenuItem("Letra");
		mnOpciones.add(mntmLetra);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmCrearVentana = new JMenuItem("Crear ventana");
		mnAyuda.add(mntmCrearVentana);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(562, 396, 89, 23);
		contentPane.add(btnCancelar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(472, 396, 89, 23);
		contentPane.add(btnAceptar);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(601, 0, 17, 385);
		contentPane.add(scrollBar);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 608, 37);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Copiar");
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cortar");
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Pegar");
		toolBar.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/images/subrayado1.png")));
		toolBar.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/images/cursiva1.png")));
		toolBar.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/images/negrita1.png")));
		toolBar.add(btnNewButton_5);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 32, 581, 341);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Formulario", null, panel, null);
		panel.setLayout(null);
		
		JRadioButton radioButton_1 = new JRadioButton("Mujer");
		buttonGroup.add(radioButton_1);
		radioButton_1.setBounds(62, 32, 102, 23);
		panel.add(radioButton_1);
		
		JRadioButton radioButton = new JRadioButton("Hombre");
		buttonGroup.add(radioButton);
		radioButton.setBounds(161, 32, 119, 23);
		panel.add(radioButton);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 11, 46, 14);
		panel.add(lblNombre);
		
		textField = new JTextField();
		textField.setBounds(66, 8, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(10, 36, 46, 14);
		panel.add(lblSexo);
		
		JLabel lblNewLabel = new JLabel("Vehiculos:");
		lblNewLabel.setBounds(10, 61, 63, 14);
		panel.add(lblNewLabel);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Bici");
		chckbxNewCheckBox.setBounds(72, 57, 102, 23);
		panel.add(chckbxNewCheckBox);
		
		JCheckBox chckbxCoche = new JCheckBox("Coche");
		chckbxCoche.setBounds(190, 57, 186, 23);
		panel.add(chckbxCoche);
		
		JLabel lblFichero = new JLabel("Fichero:");
		lblFichero.setBounds(10, 86, 46, 14);
		panel.add(lblFichero);
		
		textField_1 = new JTextField();
		textField_1.setBounds(66, 83, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnExaminar = new JButton("Examinar");
		btnExaminar.setBounds(164, 82, 89, 23);
		panel.add(btnExaminar);
		
		JLabel lblComentarios = new JLabel("Comentarios:");
		lblComentarios.setBounds(10, 144, 76, 14);
		panel.add(lblComentarios);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(96, 114, 247, 98);
		panel.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerDateModel(new Date(1517007600000L), new Date(1514847600000L), new Date(1522879200000L), Calendar.DAY_OF_YEAR));
		spinner_1.setBounds(50, 219, 63, 20);
		panel.add(spinner_1);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setBounds(10, 222, 46, 14);
		panel.add(lblFecha);
		
		JLabel label = new JLabel("Ciudad:");
		label.setBounds(10, 258, 46, 14);
		panel.add(label);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Madrid", "Barcelona", "Zaragoza", "Bilbao"}));
		comboBox.setBounds(61, 254, 119, 22);
		panel.add(comboBox);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Editor", null, panel_1, null);
		panel_1.setLayout(null);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(77, 48, 68, 29);
		panel_1.add(spinner);
		
		JSlider slider = new JSlider();
		slider.setValue(30);
		slider.setBackground(Color.BLACK);
		slider.setForeground(Color.ORANGE);
		slider.setMajorTickSpacing(10);
		slider.setToolTipText("");
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(77, 137, 200, 45);
		panel_1.add(slider);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setValue(50);
		progressBar.setBounds(77, 242, 174, 14);
		panel_1.add(progressBar);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Imagenes", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/images/cd.jpg")));
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton_6.setBounds(35, 34, 221, 222);
		panel_2.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("New button");
		btnNewButton_7.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/images/unnamed.png")));
		btnNewButton_7.setBounds(303, 34, 230, 242);
		panel_2.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("Texto Azul");
		btnNewButton_8.setForeground(Color.BLUE);
		btnNewButton_8.setBackground(Color.RED);
		btnNewButton_8.setBounds(0, 0, 377, 23);
		panel_2.add(btnNewButton_8);
	}
}
